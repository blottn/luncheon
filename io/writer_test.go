package io

import "bytes"
import "testing"

import "luncheon/model"

// TestWriteCustomers verifies the output of a call to WriteCustomers
// will produce some pre-expected output
func TestWriteCustomers(t *testing.T) {
    customers := model.Customers{
        model.Customer{0,"nick",45,-45},
        model.Customer{1,"blott",90,-90},
    }

    expected := "0:nick\n1:blott\n"

    buffer := new(bytes.Buffer)
    err := WriteCustomers(customers, buffer)
    if err != nil {
        t.Errorf("WriteCustomers returned unexpected error: customers: %v, error: %v", customers, err)
    }

    if buffer.String() != expected {
        t.Errorf("WriteCustomers wrote unexpected output: expected %s, got %s", expected, buffer.String())
    }
}
