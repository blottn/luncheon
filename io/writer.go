package io

import "fmt"
import "io"

import "luncheon/model"

// WriteCustomers outputs a series a Customers object to a writer
// each customer is output as user_id:name and a new line after each
func WriteCustomers(customers model.Customers, writer io.Writer) error {
    for _, customer := range customers {
        output := fmt.Sprint(customer.UserId) + ":" + customer.Name
        _, err := writer.Write([]byte(output))
        if err != nil {
            return err
        }
        _, err = writer.Write([]byte{'\n'})
        if err != nil {
            return err
        }
    }
    return nil
}
