package io

import "strings"
import "testing"

import "luncheon/model"


// TestReadCustomers verifies the expected output customers of parsing
// a list of customer JSONs
func TestReadCustomers(t *testing.T) {
    expected := model.Customers{
        model.Customer{0,"nick",45,-45},
        model.Customer{1,"blott",90,-90},
    }

    txt := "{\"user_id\":0,\"Name\":\"nick\",\"Latitude\":\"45\",\"Longitude\":\"-45\"}\n{\"user_id\":1,\"Name\":\"blott\",\"Latitude\":\"90\",\"Longitude\":\"-90\"}"


    customers, err := ReadCustomers(strings.NewReader(txt))

    if err != nil {
        t.Errorf("ReadCustomers returned unexpected error: input text: %s, error: %v", txt, err)
    }


    for i := range expected {
        if !expected[i].Equal(customers[i]) {
            t.Errorf("ReadCustomers produced incorrect customer at index %d: want %v, got %v", i, expected[i], customers[i])
        }
    }
}
