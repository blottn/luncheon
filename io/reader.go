package io

import "encoding/json"
import "io/ioutil"
import "io"
import "strings"

import "luncheon/model"

// ReadCustomers handles collecting customer data from a specified reader (file normally)
// by unmarhsaling JSON from each line
func ReadCustomers(reader io.Reader) (model.Customers, error) {
    bytes, err := ioutil.ReadAll(reader)
    if err != nil {
        return nil, err
    }
    customers := make(model.Customers, 0)
    for _, line := range(strings.Split(string(bytes), "\n")) {
        customer := model.Customer{}
        err := json.Unmarshal([]byte(line), &customer)
        if err != nil {
            return nil, err
        }
        customers = append(customers, customer)
    }
    return customers, nil
}
