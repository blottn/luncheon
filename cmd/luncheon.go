package main

import "flag"
import "fmt"
import "os"
import "sort"

import "luncheon/geospatial"
import "luncheon/io"
import "luncheon/model"


// distanceToIntercom is a utility function 
// it provides a partial application of arcdistance
// with intercomdublin and earthradius parameters
func distanceToIntercom(customer geospatial.Point) float64 {
    dist := geospatial.ArcDistance(customer, geospatial.IntercomDublin,
        geospatial.EarthRadius)
    return dist
}


func main() {
    // setup flags
    customerFile := flag.String("customers-file", "customers.txt",
        "The file where customer jsons are stored")
    maxDistance := flag.Int("max-distance", 100000,
        "The maximum distance in metres that a customer can be to receive an invite")
    flag.Parse()

    // Read customers
    file, err := os.Open(*customerFile)
    if err != nil {
        fmt.Println(err.Error())
    }
    defer file.Close()
    customers, err := io.ReadCustomers(file)
    if err != nil {
        fmt.Println(err.Error())
    }

    // Filter by distance
    filtered := make(model.Customers, 0)
    for _, customer := range(customers) {
        dist := distanceToIntercom(geospatial.Point{
            geospatial.DegToRad(customer.Longitude),
            geospatial.DegToRad(customer.Latitude),
        })
        if int(dist) < *maxDistance {
            filtered = append(filtered, customer)
        }
    }

    // Sort
    sort.Sort(filtered)

    // Write customers out
    err = io.WriteCustomers(filtered, os.Stdout)
    if err != nil {
        fmt.Println(err.Error())
    }
}
