package model

import "encoding/json"
import "testing"

// Represents an expected customer output for a given input json string
type customerUnmarshalTestCase struct {
    inputJSON string
    output Customer
}

// TestCustomerUnmarshal iterates over test cases and verifies they run
// run as expected
func TestCustomerUnmarshal(t *testing.T) {
    testCases := []customerUnmarshalTestCase{
        {
            "{\"latitude\": \"52.986375\", \"user_id\": 12, \"name\": \"Nicholas Blott\", \"longitude\": \"-6.043701\"}",
            Customer{12,"Nicholas Blott", 52.986375, -6.043701},
        },
        {
            "{\"latitude\": \"22.986375\", \"user_id\": 666, \"name\": \"Bruce Wayne\", \"longitude\": \"-6\"}",
            Customer{666,"Bruce Wayne", 22.986375, -6},
        },
    }
    for _, testCase := range(testCases) {
        customer := Customer{}
        err := json.Unmarshal([]byte(testCase.inputJSON), &customer)
        if err != nil {
            t.Error(err)
        }
        if !customer.Equal(testCase.output) {
            t.Errorf("Customer unmarshal produced unexpected output: case %v, got %v",
                testCase, customer)
        }
    }
}

// TestCustomersLen creates a customers struct of known length
// and gradually shortens it and verifies that the output of Len() matches
func TestCustomersLen(t *testing.T) {
    customers := Customers{
        {0, "Nicholas Blott", 123.456, 42.432},
        {1, "Nicholas Blott", 123.456, 42.432},
        {2, "Nicholas Blott", 123.456, 42.432},
        {3, "Nicholas Blott", 123.456, 42.432},
        {6, "Nicholas Blott", 123.456, 42.432},
        {5, "Nicholas Blott", 123.456, 42.432},
        {4, "Nicholas Blott", 123.456, 42.432},
    }

    for length := 7 ;length > 0; length-- {
        if length != customers.Len() {
            t.Errorf("Customers.Len() returned incorrect length, customers: %v got length %d", customers, customers.Len())
        }
        customers = customers[:length-1]
    }
}

// TestCustomersLess creates a customer struct of known ordering and
// for each i and i + 1 verifies the output is as expected in the results
// array at index i
func TestCustomersLess(t *testing.T) {
    customers := Customers{
        {0, "Nicholas Blott", 123.456, 42.432},
        {1, "Nicholas Blott", 123.456, 42.432},
        {2, "Nicholas Blott", 123.456, 42.432},
        {3, "Nicholas Blott", 123.456, 42.432},
        {6, "Nicholas Blott", 123.456, 42.432},
        {5, "Nicholas Blott", 123.456, 42.432},
        {4, "Nicholas Blott", 123.456, 42.432},
    }
    results := []bool {true,true,true,true,false,false}
    for i := 0; i+1 < len(customers); i++ {
        if customers.Less(i,i+1) != results[i] {
            t.Errorf("Customers.Less() returned incorrect ordering, customers %v indexes (%d,%d) expected %v", customers, i, i+1, results[i])
        }
    }
}

// TestCustomersSwap creates a customers struct of known order and swaps
// using all of the fields, it then verifies that each of the indices has
// the expected resulting object
func TestCustomersSwap(t *testing.T) {
    nicholas0 := Customer{0, "Nicholas Blott", 123.456, 42.432}
    nicholas1 := Customer{0, "Nicholas Blott", 123.456, 42.432}
    nicholas2 := Customer{0, "Nicholas Blott", 123.456, 42.432}

    customers := Customers{nicholas0,nicholas1,nicholas2}

    customers.Swap(0,1)
    customers.Swap(2,0)

    if !customers[0].Equal(nicholas2) &&
        !customers[1].Equal(nicholas0) &&
        !customers[2].Equal(nicholas1) {
            t.Errorf("Customers.Swap() returned incorrect ordering of elements: got %v wanted %v,%v,%v", customers, nicholas2, nicholas0, nicholas1)
    }
}
