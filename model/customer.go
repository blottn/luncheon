package model

// Customer struct stores userid, name, longitude and latitude
// And the json tags to marshal/ unmarshal
type Customer struct {
    UserId int64 `json:"user_id"`
    Name string
    Latitude float64 `json:",string"`
    Longitude float64 `json:",string"`
}


// Equal compares 2 customers by value and only returns true if all
// fields match
func (a Customer) Equal(b Customer) bool {
    return a.UserId == b.UserId &&
        a.Name == b.Name &&
        a.Latitude == b.Latitude &&
        a.Longitude == b.Longitude
}

// Customers allows a slice of Customer(s) to be sorted
type Customers []Customer

// Implementation of sort.Interface
func (customers Customers) Len() int {
    return len(customers)
}

func (customers Customers) Less(i, j int) bool {
    return customers[i].UserId < customers[j].UserId
}

func (customers Customers) Swap(i, j int) {
    temp := customers[j]
    customers[j] = customers[i]
    customers[i] = temp
}
