# Luncheon

## Description
Luncheon collects a group of customers from a file, filters by distance from
the intercom dublin office and then outputs them to stdout, sorted by user\_id.

## Packages

### `model`
model holds the type definition for a `Customer`, and therefore also
the json serialisation definition.
It also defines `Customers` and the interface functions to sort them;
(`Len()`, `Less(i, j int) bool` and `Swap(i, j int)`)

### `io`
IO provides 2 functions:
`ReadCustomers`: Reads and JSON unmarshals customer models
`WriteCustomers`: JSON marshals and writes customer models

### `geospatial`
geospatial encapsulates the trigonometry required to calculate arc distances.
It does so with the definition of a `Point` and `deltaPoint` and helper
mathematical functions for working with them; notably
(`arcAngle(from, to Point) float64`, `DegToRad(degrees float64) float64`)

### `cmd`
cmd has the main function in `luncheon.go` which ties together the other
packages to solve the desired problem. It also defines command line flags
to allow configuration of maximum customer distance and filename to read
from.

## Installation/ Running
* Install go according to the [website](https://golang.org/doc/install)
* use `go env` to find your GOPATH and navigate to `$GOPATH/src`
* `git clone git@gitlab.com:blottn/luncheon.git`
* `cd luncheon`
* `go test ./...`
* `go run cmd/luncheon.go > output2.txt`
* verify output.txt and output2.txt

## Known Issues
_sigh_ so much stuff I wish I had more time to clean up.

* Longitude and latitude are the wrong way around in the point type (caused many issues)
* No configurable output JSON mask
* Can't stream input (has to run stop run stop)
* No proper logging, print line is only so good. Though may yet fix this.


