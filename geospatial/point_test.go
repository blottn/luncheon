package geospatial

import "math"
import "testing"

// Represents a test run to verify the output arc angle
// between "inputFrom" and "inputTo"
type arcAngleTestCase struct {
    inputFrom Point
    inputTo Point
    outputAngle float64
}

// TestArcAngle loops over several instances of arcAngleTestCase and
// verifies their output
func TestArcAngle(t *testing.T) {
    testCases := []arcAngleTestCase{
        {
            inputFrom: Point{0,0},
            inputTo: Point{0,0},
            outputAngle: 0,
        },
        {
            inputFrom: Point{0,0},
            inputTo: Point{DegToRad(90),0},
            outputAngle: DegToRad(90),
        },
        {
            inputFrom: Point{0,0},
            inputTo: Point{0,DegToRad(90)},
            outputAngle: DegToRad(90),
        },
        {
            inputFrom: Point{DegToRad(45),DegToRad(45)},
            inputTo: Point{DegToRad(225),DegToRad(-45)},
            outputAngle: DegToRad(180),
        },
    }

    for _, testCase := range(testCases) {
        angle := arcAngle(testCase.inputFrom, testCase.inputTo)
        if angle != testCase.outputAngle {
            t.Errorf("arcAngle returned unexpected angle: case %v, got %f",
                testCase, angle)
        }
    }
}

// Represents a test of checking the expected difference of angle between
// 2 points on a sphere
type newDeltaTestCase struct {
    inputFrom Point
    inputTo Point
    outputDelta deltaPoint
}

// TestNewDelta loops over the test cases and verifies the expected output
func TestNewDelta(t *testing.T) {
    testCases := []newDeltaTestCase{
        {
            inputFrom: Point{0,0},
            inputTo: Point{0,0},
            outputDelta: deltaPoint{0,0},
        },
        {
            inputFrom: Point{0,0},
            inputTo: Point{0,DegToRad(90)},
            outputDelta: deltaPoint{0,DegToRad(90)},
        },
        {
            inputFrom: Point{0,0},
            inputTo: Point{DegToRad(90),0},
            outputDelta: deltaPoint{DegToRad(90),0},
        },
        {
            inputFrom: Point{0,0},
            inputTo: Point{DegToRad(90),DegToRad(90)},
            outputDelta: deltaPoint{DegToRad(90),DegToRad(90)},
        },
        {
            inputFrom: Point{0,0},
            inputTo: Point{DegToRad(-90),DegToRad(90)},
            outputDelta: deltaPoint{DegToRad(-90),DegToRad(90)},
        },
        {
            inputFrom: Point{0,0},
            inputTo: Point{DegToRad(45),DegToRad(30)},
            outputDelta: deltaPoint{DegToRad(45),DegToRad(30)},
        },
        {
            inputFrom: Point{DegToRad(45),DegToRad(30)},
            inputTo: Point{DegToRad(90),DegToRad(-20)},
            outputDelta: deltaPoint{DegToRad(45),DegToRad(-50)},
        },
        {
            inputFrom: Point{DegToRad(5),DegToRad(0.01)},
            inputTo: Point{DegToRad(90),DegToRad(-0.02)},
            outputDelta: deltaPoint{DegToRad(85),DegToRad(-0.03)},
        },
    }
    for _, testCase := range testCases {
        delta := newDelta(testCase.inputFrom, testCase.inputTo)
        if !delta.equal(&testCase.outputDelta, 0.0000000000001) {
            t.Errorf("newDelta returned unexpected angle: case %v, got %v",
                testCase, delta)
        }
    }
}

// Represents an expected output angle in radians for a given input angle
// in degrees
type degToRadTestCase struct {
    inputDegrees float64
    outputRadians float64
}

// TestDegToRad iterates over the test cases and verifies the output
// for each case
func TestDegToRad(t *testing.T) {
    testCases := []degToRadTestCase{
        {0, 0},
        {180, math.Pi},
        {360, 2 * math.Pi},
    }

    for _, testCase := range testCases {
        if DegToRad(testCase.inputDegrees) != testCase.outputRadians {
            t.Errorf("DegToRad returned unexpected result: case %v, got: %f",
                testCase,
                DegToRad(testCase.inputDegrees))
        }

    }
}
