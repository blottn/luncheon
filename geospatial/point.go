package geospatial

import "math"

// Point represents a loction on a sphere given a specific
// longitude and latitude in radians
// TODO flip order of longitude and latitude for sanity/ type them
type Point struct {
    Longitude float64
    Latitude float64
}

// Calculates arcAngle based on https://en.wikipedia.org/wiki/Great-circle_distance
func arcAngle(from, to Point) float64 {
    deltaAngles := newDelta(from, to)
    sinProduct := math.Sin(from.Latitude) * math.Sin(to.Latitude)
    cosProduct := math.Cos(from.Latitude) * math.Cos(to.Latitude) * math.Cos(deltaAngles.Longitude)
    return math.Acos(sinProduct + cosProduct)
}

// deltaPoint represents the difference of angles between 2 Points
// Eg Deltapoint of (90, 45) and (10, 60) is (-80, 15)
type deltaPoint Point

// NewDelta creates a new deltaPoint from to other points by subtracting the latter from the former
func newDelta(from, to Point) *deltaPoint {
    delta := new(deltaPoint)
    delta.Longitude = to.Longitude - from.Longitude
    delta.Latitude = to.Latitude - from.Latitude
    return delta
}

// Compare to deltas by value with a tolerance level
func (point *deltaPoint) equal(comparison *deltaPoint, tol float64) bool {
    return math.Abs(point.Longitude - comparison.Longitude) < tol &&
        math.Abs(point.Latitude - comparison.Latitude) < tol
}

// DegToRad converts a float64 in degrees to radians
func DegToRad(degrees float64) float64 {
    return degrees * (math.Pi / 180)
}
