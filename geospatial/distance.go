package geospatial

// TODO implement other functions to calculate arcdistance as
// This variant has large errors when points are close together

// ArcDistance calculates the distance between 2 points on a 
// sphere of radius r using the first formula of
// https://en.wikipedia.org/wiki/Great-circle_distance
func ArcDistance(from, to Point, r float64) float64 {
    return arcAngle(from, to) * r
}
