package geospatial

// EarthRadius is the radius of the Earth in metres
var EarthRadius = 6371008.8

// Intercom dublin office coordinates
var IntercomDublin = Point{DegToRad(-6.257664),DegToRad(53.339428)}
