package geospatial

import "math"
import "testing"

// Represents an expected output from an arcdistance calculation
// between points "from" and "to" with radius "radius"
type arcDistanceTestCase struct {
    from, to Point
    radius float64
    arcDistance float64
}


// TestArcDistance loops over several test cases and verifies the
// answer is as expected
func TestArcDistance(t *testing.T) {
    testCases := []arcDistanceTestCase{
        {
            from: Point{0, 0},
            to: Point{0, 0},
            radius: 1.0,
            arcDistance: 0,
        },
        {
            from: Point{0,0},
            to: Point{0, DegToRad(180)},
            radius: 1.0,
            arcDistance: math.Pi,
        },
        {
            from: Point{0,0},
            to: Point{0, DegToRad(45)},
            radius: 12345,
            arcDistance: math.Pi * 12345 * 0.25,
        },
    }

    for _, testCase := range testCases {
        arcDistance := ArcDistance(testCase.from, testCase.to, testCase.radius)
        if arcDistance != testCase.arcDistance {
            t.Errorf("Unexpected ArcDistance result: case %v, got %f",
                testCase, arcDistance)
        }
    }
}
